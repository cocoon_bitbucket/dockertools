dockertools
===========

tools for docker ( fig ...)

initialize a dockertools environement on a docker host

do this only once to create the data container (dockertools_data_1)

```
docker pull cocoon/dockertools:latest
docker run -ti -v /var/run/docker.sock:/var/run/docker.sock -v /tmp:/tmp cocoon/dockertools /bin/bash /opt/dockertools/bin/make-dockertools-data.sh

```

this command has created a dockertools_data_1 data container
with volumes

- projects
- data


each time you need a dockertools console : enter the following command from the docker host prompt

```
docker run --name dockertools -ti -v /var/run/docker.sock:/var/run/docker.sock --volumes-from dockertools_data_1 -v /tmp:/tmp -v $(which docker):$(which docker) cocoon/dockertools
```



custom tools
============

coming soon ...


extract files of container s volumes
inject files in a containers s volumes




make a project local
duplicate image into a local registry
generate fig project to match it


dckt image sync [--project <project_dir>]   <container> 
