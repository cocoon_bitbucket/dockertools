__author__ = 'cocoon'

import os
import re
from docker.client import Client



def scan_dockerfile_for_image(fh):
    """
    extract base image name  from dockerfile

    :param fh:
    :return:
    """
    image_pattern = re.compile(r'\s*FROM \s*(.*)\s*')

    for line in fh.readlines():
        match = image_pattern.match(line.strip())
        if match:
            return match.group(1)


def scan_fig_for_images(fh):
    """
    extract images names from an yml file

    :param fh:
    :return:
    """
    images = set()
    image_pattern = re.compile(r'\s*image: \s*(.*)\s*')

    for line in fh.readlines():
        match = image_pattern.match(line.strip())
        if match:
            images.add( match.group(1))
    return list(images)






def convert_fig_to_local(fh,dockerhub_to_local):
    """

    :param fh:
    :param dickerhub_to_local:
    :return:
    """
    image_pattern = re.compile(r'\s*image: \s*(.*)\s*')

    for line in fh.readlines():
        match = image_pattern.match(line.strip())
        if match:
            origin_image = match.group(1)
            new_line = line.replace( origin_image,dockerhub_to_local[origin_image])
            yield new_line
        else:
            yield line





def convert_images_to_registry(images,registry):
    """
    return a dictionary dockerhub to local registry image

    :param images: list , list of docker hub images
    :registry: str : the registry eg localhost:2835
    :return:
    """
    converter = {}
    for name in images:
        new_name = name.replace('/','-')
        converter[name] = "%s/%s" % (registry,new_name)
    return converter




fh = file("../samples/data/Dockerfile")
image = scan_dockerfile_for_image(fh)


fh = file("../samples/fig.yml")
images = scan_fig_for_images(fh)



images = []
for root,dirs,files in os.walk('../samples'):

    print root
    print dirs
    print files

    for filename in files:
        if filename == "Dockerfile":
            images.append(scan_dockerfile_for_image(file(os.path.join(root,filename))))
        elif filename.endswith('.yml'):
            images.extend(scan_fig_for_images(file(os.path.join(root,filename))))
    continue

uniques = list(set(images))
print uniques


converter = convert_images_to_registry(uniques,"localhost:2835")


fig = file("../samples/fig.yml")

for line in convert_fig_to_local(fig,converter):
    print line,


docker_host = os.environ.get('DOCKER_HOST',None)

if not docker_host:
    docker_host = "tcp://192.168.59.103:2375"


docker = Client(base_url=docker_host)


# get list of containers
containers = docker.containers(all=True)


# remove containers whose name begins with samples_
for container in containers:
    if container['Names'][0].startswith('/samples_'):
        docker.remove_container(container['Id'])

# get images
images = docker.images()


data = docker.inspect_container('samples_data_1')
volumes = data['Volumes']

projects = volumes['projects']


# on the host machine , acces the volume content
# projects sample :  /mnt/sda1/var/lib/docker/vfs/dir/0f63908b1e57c5d5a746b960a79805491af7025e439c098261f26f3b8120526e



#
# transfer an image to a registry
#
# tag cocoon/dev localhost:9999/dev
# push locahost:9999/dev
image = "cocoon/dev"
repository = "localhost:9999/cocoon-dev"

docker.tag(image,repository)

docker.push(repository)
